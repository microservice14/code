
First launch and getting started with git.
```sh
git config --global user.name "string"
git config --global user.email "string"  
git init
git remote add name link
```  
* * * * * 
Getting a local repository branch and changing it.
```sh
git add .
git commit -m "string"
```  
Creating a branch  
* * * * * 
```sh
git branch development 
git status 
git checkout development 
```   
* * * * * 
Writing from a local point to the server. 
```sh
git push -u name development
```  

* * * * *
Merging branch 
```sh
git checkout master  
git status  
git merge development 
git push -u name master
```   
* * * * *
Downloading code from someone else's repository (fork) 
```sh
git clone link
```  
 
* * * * *
First job in a team
```sh
git clone link 
git remote add nameRemote link 
git branch task  
git checkout task 
git add . 
git commit -m "Init commit"  
git push -u nameRemote task
```  

* * * * *
Support for current changes
```sh
git checkout master  
git pull 
git checkout task 
git merge master 
``` 
perform new developments 
```sh
git checkout master  
git pull 
git checkout task 
git merge master 
``` 